package com.o2.iris.territoryallocation.zoneTests;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import com.o2.iris.territoryallocation.model.Iris;
import com.o2.iris.territoryallocation.web.controller.ZoneController;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
@ContextConfiguration(classes = ZoneController.class)
public class ZoneTests {
	@Mock
	ZoneController zoneMock = mock(ZoneController.class);
	
	@Test
	public void zoneByIdTest() {
		Iris iris1 = new Iris();
		Iris iris2 = new Iris();
		Iris iris3 = new Iris();
		
		ArrayList<Iris> zones = new ArrayList<Iris>();
		zones.add(iris1);
		zones.add(iris2);
		zones.add(iris3);

		when(zoneMock.findById(1)).thenReturn(Optional.of(zones.get(0)));
		assertEquals(iris1,zoneMock.findById(1).get());
	}
}
