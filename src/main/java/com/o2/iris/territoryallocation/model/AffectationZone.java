package com.o2.iris.territoryallocation.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "affectation_zone")
public class AffectationZone implements Serializable{
 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="agency_id")
	@JsonManagedReference
	private Agency agency;
	

	@ManyToMany(fetch = FetchType.EAGER, cascade =
        {
        		CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH
        })
	@JoinTable(name="az_hsTypes",
	joinColumns = {@JoinColumn(name="az_id")},
	inverseJoinColumns = {@JoinColumn(name="hs_type_id")})
	@JsonManagedReference
	private Set<HouseServiceType> houseServiceTypes;
	
	@ManyToOne
	@JoinColumn(name="zone_id")
	@JsonManagedReference
	private Zone zone;
	
	
	private LocalDate start_datetime;
	
	private LocalDate end_datetime;

	public AffectationZone(Integer id, Agency agency, Set<HouseServiceType> houseServiceTypes, Zone zone,
			LocalDate start_datetime, LocalDate end_datetime) {
		this.id = id;
		this.agency = agency;
		this.houseServiceTypes = houseServiceTypes;
		this.zone = zone;
		this.start_datetime = start_datetime;
		this.end_datetime = end_datetime;
	}
	
	public AffectationZone(Agency agency, Set<HouseServiceType> houseServiceTypes, Zone zone,
			LocalDate start_datetime, LocalDate end_datetime) {
		this.agency = agency;
		this.houseServiceTypes = houseServiceTypes;
		this.zone = zone;
		this.start_datetime = start_datetime;
		this.end_datetime = end_datetime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Agency getAgency() {
		return agency;
	}

	public void setAgency(Agency agency) {
		this.agency = agency;
	}

	public Set<HouseServiceType> getHouseServiceTypes() {
		return houseServiceTypes;
	}

	public void setHouseServiceTypes(Set<HouseServiceType> houseServiceTypes) {
		this.houseServiceTypes = houseServiceTypes;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public LocalDate getStart_datetime() {
		return start_datetime;
	}

	public void setStart_datetime(LocalDate start_datetime) {
		this.start_datetime = start_datetime;
	}

	public LocalDate getEnd_datetime() {
		return end_datetime;
	}

	public void setEnd_datetime(LocalDate end_datetime) {
		this.end_datetime = end_datetime;
	}
	
	@JsonIgnore
	public List<String> getHsTypeIdList(){
		ArrayList<String> res = new ArrayList<>();
		for (HouseServiceType hst : getHouseServiceTypes()) {
			res.add(hst.getShortname());
		}
		return res;
	}

	@Override
	public String toString() {
		return "AffectationZone [id=" + id + ", agency=" + agency + ", houseServiceTypes=" + houseServiceTypes
				+ ", zone=" + zone + ", start_datetime=" + start_datetime + ", end_datetime=" + end_datetime + "]";
	}

	public AffectationZone() {
		super();
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AffectationZone other = (AffectationZone) obj;
		if (agency == null) {
			if (other.agency != null)
				return false;
		} else if (!agency.equals(other.agency))
			return false;
		if (end_datetime == null) {
			if (other.end_datetime != null)
				return false;
		} else if (!end_datetime.equals(other.end_datetime))
			return false;
		if (houseServiceTypes == null) {
			if (other.houseServiceTypes != null)
				return false;
		} else if (!houseServiceTypes.equals(other.houseServiceTypes))
			return false;
		if (start_datetime == null) {
			if (other.start_datetime != null)
				return false;
		} else if (!start_datetime.equals(other.start_datetime))
			return false;
		if (zone == null) {
			if (other.zone != null)
				return false;
		} else if (!zone.equals(other.zone))
			return false;
		return true;
	}
	
	
	
	
}
