package com.o2.iris.territoryallocation.agencyTests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
//import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.o2.iris.territoryallocation.model.Agency;
import com.o2.iris.territoryallocation.web.controller.AgencyController;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
@ContextConfiguration(classes = AgencyController.class)
public class AgencyTests {
	
	@Mock
	AgencyController agencyMock = mock(AgencyController.class);
	
	@Test
	public void agencyByIdTest() {
		ArrayList<Agency> agencies = new ArrayList<Agency>();
		agencies.add(new Agency(1,"O2 Lille Est"));
		agencies.add(new Agency(2,"O2 Alençon"));
		agencies.add(new Agency(3,"O2 Issy-Les-Moulineaux"));

		when(agencyMock.getAgenciesByIdOdy(1)).thenReturn(Optional.of(agencies.get(0)));
		assertEquals(new Agency(1,"O2 Lille Est"),agencyMock.getAgenciesByIdOdy(1).get());
	}
	
	@Test
	public void agencyNameById() {
		when(agencyMock.getAgenciesNameById(1)).thenReturn(Optional.of("O2 Lille Est"));
		assertEquals("O2 Lille Est",agencyMock.getAgenciesNameById(1).get());
	}
	
	@Test
	public void allAgenciesTest() {
		ArrayList<Agency> agencies = new ArrayList<Agency>();
		agencies.add(new Agency(1,"O2 Lille Est"));
		agencies.add(new Agency(2,"O2 Alençon"));
		agencies.add(new Agency(3,"O2 Issy-Les-Moulineaux"));
		
		when(agencyMock.getAllAgencies()).thenReturn(agencies);
		assertEquals(agencies,agencyMock.getAllAgencies());
	}
}
