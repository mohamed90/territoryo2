package com.o2.iris.territoryallocation.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.o2.iris.territoryallocation.model.AffectationZone;
import com.o2.iris.territoryallocation.model.HouseServiceType;

@Repository
public interface AffectationZoneRepository extends JpaRepository<AffectationZone, Integer> {

	@Query(value="SELECT af_zone FROM AffectationZone AS af_zone " + 
			"LEFT JOIN Zone as zone ON af_zone.zone.id=zone.id " + 
			"WHERE function('MBRCONTAINS', zone.coordinates, function('ST_GEOMFROMTEXT', 'POINT(' || :lon || ' ' || :lat || ')')) = TRUE ")
	List<AffectationZone> findAffectationZonesByCoordinates(@Param("lon") Double longitude
			, @Param("lat") Double latitude);

	@Query(value="SELECT af_zone FROM AffectationZone AS af_zone " + 
			"	LEFT JOIN Agency AS agcy ON af_zone.agency.id=agcy.id " + 
			"	WHERE agcy.id =:agencyId")
	List<AffectationZone> getAllAffectationZonesByAgency(@Param("agencyId")Integer agencyId);
	
	@Query(value="SELECT af_zone.id FROM AffectationZone AS af_zone " + 
			"	LEFT JOIN Agency AS agcy ON af_zone.agency.id=agcy.id " + 
			"	WHERE agcy.id =:agencyId")
	List<Integer> getAllAffectationZoneIdsByAgency(@Param("agencyId")Integer agencyId);
	
	@Query(value="SELECT af_zone.id FROM AffectationZone AS af_zone " + 
			"	LEFT JOIN Agency AS agcy ON af_zone.agency.id=agcy.id " + 
			"	LEFT JOIN Zone AS zone ON af_zone.zone.id=zone.id " + 
			"	WHERE agcy.id =:agencyId AND zone.id=:zoneId")
	List<Integer> getAllAffectationZoneIdsByAgencyAndZone(@Param("agencyId")Integer agencyId, @Param("zoneId")Integer zoneId);

	@Query(value="SELECT COUNT(*) FROM affectation_zone,agency " + 
	" WHERE agency.id = affectation_zone.agency_id " + 
	" AND agency.id_ody =:idOdy", nativeQuery=true)
	Long countZonesByAgencyOdy(@Param("idOdy")Integer idOdy);

	@Modifying
	@Transactional
	@Query(value="DELETE af_zone FROM affectation_zone AS af_zone " + 
			"	LEFT JOIN agency AS agcy ON af_zone.agency_id=agcy.id " + 
			"	WHERE agcy.id =:agencyId", nativeQuery=true )
	void deleteAffectationZoneByAgcy(@Param("agencyId")Integer agencyId);

	@Modifying
	@Transactional
	@Query(value="DELETE af_zone FROM affectation_zone AS af_zone " + 
			"	LEFT JOIN zone AS zone ON af_zone.zone_id=zone.id " + 
			"	LEFT JOIN agency AS agcy ON af_zone.agency_id=agcy.id " + 
			"	WHERE zone.id =:zoneId AND agcy.id =:agencyId", nativeQuery=true )
	void deleteAffectationZoneByAgcy_Zone(@Param("zoneId")Integer zoneId
			, @Param("agencyId")Integer agencyId);

	@Modifying
	@Transactional
	@Query(value="DELETE azhst.* FROM az_hs_types AS azhst " + 
			"WHERE azhst.az_id IN :azhstIds", nativeQuery=true )
	void deleteJoinedTable(@Param("azhstIds")List<Integer> azhstIds);

	
	@Query(value="SELECT DISTINCT az FROM AffectationZone az, HouseServiceType hst WHERE az.zone.id = :zone_id" + 
			"    AND hst IN (:hst) AND hst IN ELEMENTS(az.houseServiceTypes)")
	Optional<AffectationZone> isHstAlreadyAffected(@Param("zone_id")Integer zone_id, @Param("hst")HouseServiceType hst);
	
	


}
