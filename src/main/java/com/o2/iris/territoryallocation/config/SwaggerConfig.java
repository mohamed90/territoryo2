package com.o2.iris.territoryallocation.config;

import java.util.Collections;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.core.env.Environment;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {
	
	@Autowired
    private Environment env;  

	@Bean
	public Docket apiDocket() {
		HashSet<String> hs = new HashSet<>();
		hs.add("http");
		hs.add("https");
		return new Docket(DocumentationType.SWAGGER_2)
				.protocols(hs)
				.host(env.getProperty("server.host") + ":" + env.getProperty("swagger.port"))
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.o2.iris.territoryallocation.web.controller"))
				.paths(PathSelectors.ant("/api/**"))
				.build()
				.apiInfo(getApiInfo());
	}

	private ApiInfo getApiInfo() {
		return new ApiInfo(
				"API O2 IRIS",
				"API permettant de récupérer les données en relation au projet IRIS",
				"1",
				"www.o2.fr",
				new Contact("Anthony Lahire","https://plus.google.com/113767361505048735416","Anthony.lahire@o2.fr"),
				"o2",
				"www.o2.fr",
				Collections.emptyList()
				);
	}
}
