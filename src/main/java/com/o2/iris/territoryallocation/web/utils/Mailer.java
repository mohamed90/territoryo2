package com.o2.iris.territoryallocation.web.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Mailer {

	private static final Logger log = LoggerFactory.getLogger(Mailer.class);
	
	public static final String sPARAM_DEST_A = "destA";
	public static final String sPARAM_DEST_FROM = "destFrom";
	public static final String sPARAM_DEST_CC = "destCC";
	public static final String sPARAM_DEST_EMETTEUR = "destEmet";
	public static final String sPARAM_DEST_REPLYTO = "destReplyTo";
	public static final String sPARAM_OBJET = "objet";
	public static final String sPARAM_MESSAGE = "msg";
	public static final String sPARAM_URL = "url";
	public static final String sPARAM_TYPE_MSG = "typeMsg";
	public static final String sPARAM_TYPE_SMS = "typeSMS";
	public static final String sPARAM_TYPE_DOCUMENT = "typeDoc";
	public static final String sPARAM_NOM_PIECE_JOINTE = "nomPJ";
	public static final String sPARAM_ID_CONTACT = "contId";
	public static final String sPARAM_SERVER_SENDER = "server";
	
	private String objet;
	
	private String message;
	
	private String from;
	
	private List<String> to;
	
	private String type;
	
	private String profile;
	
	private String telegestUrl;

	public Mailer(String objet, String message, String from, List<String> to) {
		super();
		this.objet = objet;
		this.message = message;
		this.from = from;
		this.to = to;
		this.type = "1";
	}

	public Mailer(String profile, String telegestUrl) {
		this.setProfile(profile);
		this.setTelegestUrl(telegestUrl);
	}
	
	public String send() {
		if(this != null && this.isConformed())
			try {
				log.info("envoi du mail : {}", this);
				HttpUtil httpUtil = new HttpUtil();
				return httpUtil.sendRequest(this);
			} catch (Exception e) {
				e.printStackTrace();
				return e.getMessage();
			}
		return "Mail non conforme";
	}

	// TODO: méthode permettant de vérifier si un mail est conforme (présence d'un destinataire, sender, objet, message)
	protected boolean isConformed() {
		return true;
	}
	
	protected String getUrlParams() throws UnsupportedEncodingException {
		String urlParams = "";
		urlParams  += "&" + sPARAM_TYPE_MSG + "=" + URLEncoder.encode(getType(), HttpUtil.sDEFAULT_CHARSET);
		urlParams  += "&" + sPARAM_OBJET + "=" + URLEncoder.encode(getObjet(), HttpUtil.sDEFAULT_CHARSET);
		urlParams  += "&" + sPARAM_MESSAGE + "=" + URLEncoder.encode(getMessage(), HttpUtil.sDEFAULT_CHARSET);
		urlParams  += "&" + sPARAM_DEST_FROM + "=" + URLEncoder.encode(getFrom(), HttpUtil.sDEFAULT_CHARSET);
		urlParams  += "&" + sPARAM_DEST_A + "=" + URLEncoder.encode(getToAsString(), HttpUtil.sDEFAULT_CHARSET);
		urlParams  += "&" + sPARAM_SERVER_SENDER + "=" + URLEncoder.encode("junit.test", HttpUtil.sDEFAULT_CHARSET);
		
		return urlParams;
	}
	
	public String getObjet() {
		return objet;
	}

	public Mailer setObjet(String objet) {
		this.objet = objet;
		return this;
	}

	public String getMessage() {
		return message;
	}

	public Mailer setMessage(String message) {
		this.message = message;
		return this;
	}

	public String getFrom() {
		return from;
	}

	public Mailer setFrom(String from) {
		this.from = from;
		return this;
	}

	public List<String> getTo() {
		return to;
	}
	
	private String getToAsString() {
		String tos = "";
		for (String dest : to) {
			tos += dest + ";";
		}
		return tos;
	}

	public Mailer setTo(List<String> to) {
		this.to = to;
		return this;
	}

	public String getType() {
		return type;
	}

	public Mailer setType(String type) {
		this.type = type;
		return this;
	}


	public String getProfile() {
		return profile;
	}

	public Mailer setProfile(String profile) {
		this.profile = profile;
		return this;
	}
	

	public String getTelegestUrl() {
		return telegestUrl;
	}

	public Mailer setTelegestUrl(String telegestUrl) {
		if(!this.profile.equals("prod") && !telegestUrl.contains("beta")) {
			log.error("Environnement différent de prod, mais appel quand même le serveur telegest de prod, attention !");
			throw new RuntimeException();
		} else {			
			this.telegestUrl = telegestUrl;
			
			return this;
		}
	}
	
	@Override
	public String toString() {
		return "Mailer [objet=" + objet + ", message=" + message + ", from=" + from + ", to=" + to + ", type=" + type
				+ "]";
	}

}
