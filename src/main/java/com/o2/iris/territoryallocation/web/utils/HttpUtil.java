package com.o2.iris.territoryallocation.web.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpUtil {

	private static final Logger log = LoggerFactory.getLogger(HttpUtil.class);
	
	
	public static final String sPARAM_DEST_A = "destA";
	public static final String sPARAM_DEST_FROM = "destFrom";
	public static final String sPARAM_DEST_CC = "destCC";
	public static final String sPARAM_DEST_EMETTEUR = "destEmet";
	public static final String sPARAM_DEST_REPLYTO = "destReplyTo";
	public static final String sPARAM_OBJET = "objet";
	public static final String sPARAM_MESSAGE = "msg";
	public static final String sPARAM_URL = "url";
	public static final String sPARAM_TYPE_MSG = "typeMsg";
	public static final String sPARAM_TYPE_SMS = "typeSMS";
	public static final String sPARAM_TYPE_DOCUMENT = "typeDoc";
	public static final String sPARAM_NOM_PIECE_JOINTE = "nomPJ";
	public static final String sPARAM_ID_CONTACT = "contId";
	public static final String sPARAM_SERVER_SENDER = "server";

	public static final String sMETHOD_GET = "GET";
	public static final String sMETHOD_POST = "POST";
	public static final String sMETHOD_DELETE = "DELETE";

	public static final String sDEFAULT_CHARSET = "UTF-8";
	public static final String sCONTENT_TYPE = "Content-type";

	public static final String sCONTENT_TYPE_URLENCODED = "application/x-www-form-urlencoded; charset=utf-8";

	public static final String sREQ_ATTR_EXTRACTED_RESPONSE = "sResponseContent";
	public static final String sREQ_ATTR_EXTRACTED_BODY = "sRequestBody";

	
	public String sendRequest(Mailer mailer) throws Exception {
		
		

		HttpURLConnection oURLConnection = invokeAndReturnHttpUrlConnection(sMETHOD_POST, mailer);
		int iResponseCode = oURLConnection.getResponseCode();
		// parcours des headers pour détection du charset
		int i = 0;
		String sRespCharset = HttpUtil.sDEFAULT_CHARSET;
		if (null != oURLConnection.getContentEncoding())
			sRespCharset = oURLConnection.getContentEncoding();
		while (oURLConnection.getHeaderFieldKey(i) == null)
			i++;
		while (oURLConnection.getHeaderFieldKey(i) != null) {
			String sKey = oURLConnection.getHeaderFieldKey(i);
			String sVal = oURLConnection.getHeaderField(i);
			if ("content-type".equalsIgnoreCase(sKey)) {
				Pattern oP = Pattern.compile("charset=(.*)(;|$)");
				Matcher oM = oP.matcher(sVal);
				if (oM.find())
					sRespCharset = oM.group(1);
			}
			i++;
		}

		if (200 != iResponseCode) {
			throw new Exception("Return code is " + iResponseCode + " (expected was " + 200
					+ ") for url " + mailer.getTelegestUrl());

		} else if (302 == iResponseCode) {
			String sRedirectUrl = oURLConnection.getHeaderField("Location");
			return sRedirectUrl;
		}
		

		// Lit la réponse du serveur (i.e. données renvoyées au client)
		BufferedInputStream oIS = new BufferedInputStream(oURLConnection.getInputStream());
		BufferedReader oBR = new BufferedReader(new InputStreamReader(oIS, sRespCharset));

		StringBuffer oSB = new StringBuffer();
		String sLine = null;
		while (null != (sLine = oBR.readLine()))
			oSB.append(sLine).append("\n");
		String sReturn = oSB.toString().trim();

		oURLConnection.disconnect();

		return sReturn;
	}

	// ------------------- invokeAndReturnHttpUrlConnection ---------------------
	/**
	 * @param p_sMethod     "GET" ou "POST"
	 * @param p_oUrl        url à appeler, éventuellement de la forme
	 *                      "user:pwd@host"
	 * @param p_hSubmitData
	 * @param p_sCookie
	 * @param p_sUserAgent  (optionnel) user agent à indiquer
	 * @return l'objet HTTPURLConnection. Attention la déconnection est à la charge
	 *         des méthodes appelantes
	 * @throws Exception si code retour <>200
	 */
	public final HttpURLConnection invokeAndReturnHttpUrlConnection(String p_sMethod, Mailer mailer) throws Exception {

		URL telegestUrl = new URL(mailer.getTelegestUrl());
		
		try {
			String sData = "";
			boolean bHasData = mailer != null && mailer.isConformed();
			// cela doit être fait avant l'appel à "openconnection" puisque pour un "GET"
			// les param sont ajouté à l'url
			if (bHasData) {
				sData = mailer.getUrlParams();
				sData = sData.substring(1);
			}

			// établissement de la connexion
			HttpURLConnection oURLConnection = (HttpURLConnection) telegestUrl.openConnection();
			oURLConnection.setDoOutput(true);
			oURLConnection.setUseCaches(false);
			{ // si il existe des info d'authentification, exploitation de ces infos
				String sUI = telegestUrl.getUserInfo();
				final String[] ms = null == sUI ? null : sUI.split(":");
				if (null != ms && 2 == ms.length) {
					String userpass = ms[0] + ":" + URLDecoder.decode(ms[1], "UTF-8");
					String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
					oURLConnection.setRequestProperty("Authorization", basicAuth);
				} else if (null != ms && 1 == ms.length) {
					String basicAuth = "Basic " + new String(new Base64().encode(ms[0].getBytes()));
					oURLConnection.setRequestProperty("Authorization", basicAuth);
				}
			}

			oURLConnection.setRequestMethod(p_sMethod);
			// cf.
			// http://www.yoyodesign.org/doc/w3c/xforms1/slice11.html#serialize-urlencode
			oURLConnection.setRequestProperty("Content-Type", sCONTENT_TYPE_URLENCODED);

			if (bHasData && sMETHOD_POST == p_sMethod) {
				oURLConnection.setDoInput(true);
				OutputStreamWriter wr = new OutputStreamWriter(oURLConnection.getOutputStream());
				wr.write(sData);
				wr.flush();
			}

			oURLConnection.connect();
			return oURLConnection;

		} catch (ConnectException eErr) {
			log.error("Failed to connect to Url : " + telegestUrl.toString(), eErr);
			throw eErr;
		}
	}
}
