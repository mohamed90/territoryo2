package com.o2.iris.territoryallocation.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;

import io.swagger.annotations.ApiModelProperty;


@Entity
@Table(name = "agency")
public class Agency implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(notes = "Identifiant unique de l'agence.", example = "1", required = true, position = 0)
	private Integer id;
	
	@NotNull
	@ApiModelProperty(notes = "Identifiant unique de l'agence sur Odyssée.", example = "1547", required = true, position = 1)
	private Integer idOdy;
	
	@JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(using = GeometryDeserializer.class)
	private Geometry coordinates;
	
	private boolean disabled;
	
	@NotNull
	@Size(max=200)
	@ApiModelProperty(notes = "Nom de l'agence", example = "O2 Le Mans", required = true, position = 1)
	private String name;
	
	@OneToMany(mappedBy="agency")
	@JsonBackReference
	private Set<AffectationZone> affectationZones;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade =
        {
        		CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH
        })
	@JoinTable(name="agency_marketed_hs_types",
	joinColumns = {@JoinColumn(name="id_ody", referencedColumnName="idOdy")},
	inverseJoinColumns = {@JoinColumn(name="hs_type_sn", referencedColumnName="shortname")})
	@JsonManagedReference
	private Set<HouseServiceType> marketedHouseServiceTypes;
	
	public Agency(Integer id, @NotNull @Size(max = 200) String name) {
		this.id = id;
		this.name = name;
	}
	
	public Set<HouseServiceType> getMarketedHouseServiceTypes() {
		return marketedHouseServiceTypes;
	}

	public void setMarketedHouseServiceTypes(Set<HouseServiceType> marketedHouseServiceTypes) {
		this.marketedHouseServiceTypes = marketedHouseServiceTypes;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<AffectationZone> getAffectationZones() {
		return affectationZones;
	}

	public void setAffectationZones(Set<AffectationZone> affectationZones) {
		this.affectationZones = affectationZones;
	}
	

	public Integer getIdOdy() {
		return idOdy;
	}

	public void setIdOdy(Integer idOdy) {
		this.idOdy = idOdy;
	}

	public Geometry getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Geometry coordinates) {
		this.coordinates = coordinates;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	

	@Override
	public String toString() {
		return "Agency [id=" + id + ", idOdy=" + idOdy + ", disabled=" + disabled + ", name=" + name + "]";
	}

	public Agency() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agency other = (Agency) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	

	
}
