package com.o2.iris.territoryallocation.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name = "house_service_type")
public class HouseServiceType implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	@Size(max=45)
	private String shortname;

	@NotNull
	@Size(max=45)
	private String libelle;

	@ManyToMany(mappedBy="houseServiceTypes", cascade =
		{
			CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH
		})
	@JsonBackReference
	private Set<AffectationZone> affectationZones;
	
	
	@ManyToMany(mappedBy="marketedHouseServiceTypes", cascade =
		{
			CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH
		})
	@JsonBackReference
	private Set<Agency> agencies;

	public HouseServiceType(Integer id, String shortname, String libelle) {
		this.id = id;
		this.shortname = shortname;
		this.libelle = libelle;
	}
	
	public Set<Agency> getAgencies() {
		return agencies;
	}

	public void setAgencies(Set<Agency> agencies) {
		this.agencies = agencies;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Set<AffectationZone> getAffectationZones() {
		return affectationZones;
	}

	public void setAffectationZones(Set<AffectationZone> affectationZones) {
		this.affectationZones = affectationZones;
	}

	@Override
	public String toString() {
		return "HouseServiceType [id=" + id + ", shortname=" + shortname + ", libelle=" + libelle + "]";
	}

	public HouseServiceType() {
		super();
	}



}
