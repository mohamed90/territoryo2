package com.o2.iris.territoryallocation.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.vividsolutions.jts.geom.Geometry;

@Entity
@Table(name = "iris")
@PrimaryKeyJoinColumn(name = "zone_id")
public class Iris extends Zone implements Serializable{
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id; 
	
	@Column(name="DEPCOM")
	private String depcom;
	
	@Column(name="NOM_COM")
	private String nom_com;
	
	@Column(name="IRIS")
	private String iris;
	
	@Column(name="DCOMIRIS")
	private String dcomiris;
	
	@Column(name="NOM_IRIS")
	private String nom_iris;
	
	@Column(name="TYP_IRIS")
	private String typ_iris;
	
	@Column(name="geo_point_2d")
	private String geo_point_2d;
	
	@Column(name="num_dept")
	private String num_dept;
	
	@Column(name="NOM_DEPT")
	private String nom_dept;
	
	@Column(name="NOM_REGION")
	private String nom_region;
	
	@Column(name="P12_POP")
	private String p12_pop;
	
	@Column(name="CODE_REGION_2016")
	private String code_region_2016;
	
	@Column(name="CODE_REGION")
	private String code_region;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDepcom() {
		return depcom;
	}

	public void setDepcom(String depcom) {
		this.depcom = depcom;
	}

	public String getNom_com() {
		return nom_com;
	}

	public void setNom_com(String nom_com) {
		this.nom_com = nom_com;
	}

	public String getIris() {
		return iris;
	}

	public void setIris(String iris) {
		this.iris = iris;
	}

	public String getDcomiris() {
		return dcomiris;
	}

	public void setDcomiris(String dcomiris) {
		this.dcomiris = dcomiris;
	}

	public String getNom_iris() {
		return nom_iris;
	}

	public void setNom_iris(String nom_iris) {
		this.nom_iris = nom_iris;
	}

	public String getTyp_iris() {
		return typ_iris;
	}

	public void setTyp_iris(String typ_iris) {
		this.typ_iris = typ_iris;
	}

	public String getGeo_point_2d() {
		return geo_point_2d;
	}

	public void setGeo_point_2d(String geo_point_2d) {
		this.geo_point_2d = geo_point_2d;
	}

	public String getNum_dept() {
		return num_dept;
	}

	public void setNum_dept(String num_dept) {
		this.num_dept = num_dept;
	}

	public String getNom_dept() {
		return nom_dept;
	}

	public void setNom_dept(String nom_dept) {
		this.nom_dept = nom_dept;
	}

	public String getNom_region() {
		return nom_region;
	}

	public void setNom_region(String nom_region) {
		this.nom_region = nom_region;
	}

	public String getP12_pop() {
		return p12_pop;
	}

	public void setP12_pop(String p12_pop) {
		this.p12_pop = p12_pop;
	}

	public String getCode_region_2016() {
		return code_region_2016;
	}

	public void setCode_region_2016(String code_region_2016) {
		this.code_region_2016 = code_region_2016;
	}

	public String getCode_region() {
		return code_region;
	}

	public void setCode_region(String code_region) {
		this.code_region = code_region;
	}

	@Override
	public String toString() {
		return "Iris [id=" + id + ", depcom=" + depcom + ", nom_com=" + nom_com + ", iris=" + iris + ", dcomiris="
				+ dcomiris + ", nom_iris=" + nom_iris + ", typ_iris=" + typ_iris + ", geo_point_2d=" + geo_point_2d
				+ ", num_dept=" + num_dept + ", nom_dept=" + nom_dept + ", nom_region=" + nom_region + ", p12_pop="
				+ p12_pop + ", code_region_2016=" + code_region_2016 + ", code_region=" + code_region + "]";
	}

	public Iris(Integer id, String name, Geometry coordinates, Integer id2, String depcom, String nom_com, String iris,
			String dcomiris, String nom_iris, String typ_iris, String geo_point_2d, String num_dept, String nom_dept,
			String nom_region, String p12_pop, String code_region_2016, String code_region) {
		super(id, name, coordinates);
		id = id2;
		this.depcom = depcom;
		this.nom_com = nom_com;
		this.iris = iris;
		this.dcomiris = dcomiris;
		this.nom_iris = nom_iris;
		this.typ_iris = typ_iris;
		this.geo_point_2d = geo_point_2d;
		this.num_dept = num_dept;
		this.nom_dept = nom_dept;
		this.nom_region = nom_region;
		this.p12_pop = p12_pop;
		this.code_region_2016 = code_region_2016;
		this.code_region = code_region;
	}

	public Iris() {
		super();
	}


	
	

}
