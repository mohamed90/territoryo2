package com.o2.iris.territoryallocation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.o2.iris.territoryallocation.model.City;

@Repository
public interface CityRepository extends JpaRepository<City, Integer>{

	
	@Query("SELECT city FROM City as city WHERE shortname=:name")
    List<City> findCitiesByName(String name);

}
