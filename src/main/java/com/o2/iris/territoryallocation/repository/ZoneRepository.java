package com.o2.iris.territoryallocation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.o2.iris.territoryallocation.model.Zone;

@Repository
public interface ZoneRepository extends JpaRepository<Zone, Integer>{
	
	@Query(value="SELECT zone FROM Zone AS zone " + 
			"WHERE function('MBRINTERSECTS', zone.coordinates, function('ST_GEOMFROMTEXT', "+ 
			"'POLYGON((" + 
			"' || :BLLng || ' ' || :TRLat || '," + 
			"' || :TRLng || ' ' || :TRLat || '," + 
			"' || :TRLng || ' ' || :BLLat || '," + 
			"' || :BLLng || ' ' || :BLLat || '," + 
			"' || :BLLng || ' ' || :TRLat || '))'" + 
			")) = TRUE ")
	List<Zone> findZoneInPolygone(
			  @Param("TRLng")Double TRLng
			, @Param("TRLat")Double TRLat
			, @Param("BLLng")Double BLLng
			, @Param("BLLat")Double BLLat);

	
}
	