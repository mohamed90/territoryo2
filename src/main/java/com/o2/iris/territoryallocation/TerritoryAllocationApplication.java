package com.o2.iris.territoryallocation;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.core.env.Environment;


@SpringBootApplication
@EnableCaching
public class TerritoryAllocationApplication {

	private static final Logger log = LoggerFactory.getLogger(TerritoryAllocationApplication.class);

	public static void main(String[] args) throws UnknownHostException {
		SpringApplication app = new SpringApplication(TerritoryAllocationApplication.class);

		Environment env = app.run(args).getEnvironment();
		String protocol = "http";
		if (env.getProperty("server.ssl.key-store") != null) {
			protocol = "https";
		}
		log.info("\n----------------------------------------------------------\n\t" +
				"Application '{}' is running! Access URLs:\n\t" +
				"Local: \t\t{}://localhost:{}\n\t" +
				"External: \t{}://{}:{}\n\t" +
				"Swagger: \t{}://{}:{}{}\n\t" +
				"Profile(s): \t{}\n\t"+
				"Datasource: \t{}"+
				"\n----------------------------------------------------------"
				, env.getProperty("spring.application.name")
				, protocol
				, env.getProperty("server.port")
				, protocol
				, InetAddress.getLocalHost().getHostAddress()
				, env.getProperty("server.port")
				, protocol
				, env.getProperty("server.host")
				, env.getProperty("server.port")
				, "/swagger-ui.html"
				, env.getActiveProfiles()
				, env.getProperty("spring.datasource.url")
				);
	}
}
