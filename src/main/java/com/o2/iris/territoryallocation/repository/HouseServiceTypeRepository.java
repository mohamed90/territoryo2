package com.o2.iris.territoryallocation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.o2.iris.territoryallocation.model.HouseServiceType;

@Repository
public interface HouseServiceTypeRepository extends JpaRepository<HouseServiceType, Integer>{
	
	HouseServiceType findByshortname(String shortname);
	
	@Query("SELECT hst FROM HouseServiceType hst"
			+ " JOIN hst.agencies a"
			+ " WHERE a.idOdy = :idOdy")
	List<HouseServiceType> findByIdOdy(@Param("idOdy")Integer id_ody);
}
