package com.o2.iris.territoryallocation.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "zone")
public abstract class Zone implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Integer id;
	
	@NotNull
	@Size(max=500)
	private String name;
	
	@JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(using = GeometryDeserializer.class)
	private Geometry coordinates;

	@OneToMany(mappedBy="zone")
	@JsonBackReference
	private Set<AffectationZone> affectationZones;

	public Zone(Integer id, String name, Geometry coordinates) {
		this.id = id;
		this.name = name;
		this.coordinates = coordinates;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Geometry getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Geometry coordinates) {
		this.coordinates = coordinates;
	}

	public Set<AffectationZone> getAffectationZones() {
		return affectationZones;
	}

	public void setAffectationZones(Set<AffectationZone> affectationZones) {
		this.affectationZones = affectationZones;
	}

	@Override
	public String toString() {
		return "Zone [id=" + id + ", name=" + name + ", coordinates=" + coordinates + "]";
	}

	public Zone() {
		super();
	}
	
	
}
