package com.o2.iris.territoryallocation.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.vividsolutions.jts.geom.Geometry;

@Entity
@Table(name = "city")
@PrimaryKeyJoinColumn(name = "zone_id")
public class City extends Zone implements Serializable{
	
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "City [id=" + id + "]";
	}

	public City(Integer id, String name, Geometry coordinates, Integer id2) {
		super(id, name, coordinates);
		id = id2;
	}

	public City(Integer id, String name, Geometry coordinates) {
		super(id, name, coordinates);
	}
	
	
}
