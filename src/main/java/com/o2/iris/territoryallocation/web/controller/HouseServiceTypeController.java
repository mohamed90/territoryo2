package com.o2.iris.territoryallocation.web.controller;

import java.net.URI;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.o2.iris.territoryallocation.model.HouseServiceType;
import com.o2.iris.territoryallocation.repository.HouseServiceTypeRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class HouseServiceTypeController {
	
	 private static final Logger log = LoggerFactory.getLogger(HouseServiceTypeController.class);
	
	@Autowired
	private HouseServiceTypeRepository houseServiceTypeRepository;

	@GetMapping(path="/house-service-types")
	@ApiOperation("Retourne tous le tyes de presta")
	@Cacheable("houseServiceTypes")
	public List<HouseServiceType> getAllHouseServiceTypes(){
		log.debug("Requête pour récupérer toutes les prestations");
		List<HouseServiceType> houseServiceTypes = houseServiceTypeRepository.findAll();
		
		Iterator<HouseServiceType> it = houseServiceTypes.iterator();
		
		while(it.hasNext()) {
			HouseServiceType hst = it.next();
			if(hst.getShortname().equals("menage") || hst.getShortname().equals("repassage") ) {
				it.remove();
			}
		}
		return houseServiceTypes;
	}
	
	@GetMapping(path="/house-service-types/{agcy_idOdy}")
	@ApiOperation("Retourne tous les types de presta par agence")
	@Cacheable(value="houseServiceTypes", key="#agcy_idOdy")
	public List<HouseServiceType> getHouseServiceTypesByAgency(@ApiParam("idOdy de l'agence")@PathVariable Integer agcy_idOdy){
		log.debug("Requête pour récupérer tous les types de prestation comercialisés par agence");
		
		List<HouseServiceType> houseServiceTypes = houseServiceTypeRepository.findByIdOdy(agcy_idOdy);
		
		Iterator<HouseServiceType> it = houseServiceTypes.iterator();
		
		while(it.hasNext()) {
			HouseServiceType hst = it.next();
			if(hst.getShortname().equals("menage") || hst.getShortname().equals("repassage") ) {
				it.remove();
			}
		}
		return houseServiceTypes;
	}
	
	@PostMapping(path="/house-service-types")
	@ApiOperation("Ajoute un type de presta")
	public ResponseEntity<?> addHouse(@RequestBody HouseServiceType houseServiceType){
		
		HouseServiceType houseServiceTypeAdded = houseServiceTypeRepository.save(houseServiceType);
		
		if(houseServiceTypeAdded == null)
			return ResponseEntity.noContent().build();
		
		 URI location = ServletUriComponentsBuilder
	                .fromCurrentRequest()
	                .path("/{id}")
	                .buildAndExpand(houseServiceTypeAdded.getId())
	                .toUri();
		 
		return ResponseEntity.created(location).build();

	}
}
