package com.o2.iris.territoryallocation.web.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.o2.iris.territoryallocation.model.Agency;
import com.o2.iris.territoryallocation.repository.AgencyRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@RestController
@RequestMapping("/api")
public class AgencyController {
	
	 private static final Logger log = LoggerFactory.getLogger(AgencyController.class);
	
	@Autowired
    private AgencyRepository agencyRepository;
	
	@GetMapping(path="/agencies")
	@ApiOperation("Retourne toutes les agences")
	@Cacheable("agencies")
	public List<Agency> getAllAgencies(){
		log.debug("Requête pour récupérer toutes les agences");
		return agencyRepository.findAll();
	}
	
	@GetMapping(path="/agencies/{id_ody}")
	@ApiOperation("Retourne une agence par id_ody")
	public Optional<Agency> getAgenciesByIdOdy(@ApiParam("Id ody de l'agence. Obligatoire.")@PathVariable Integer id_ody) {
		log.debug("Requête pour récupérer une agence à partir de son id : {}", id_ody);
		return agencyRepository.findByidOdy(id_ody);
	}
	@GetMapping(path="/agencies/name/{id}")
	@ApiOperation("Retourne le nom d'une agence en fonction de son Id")
	public Optional<String> getAgenciesNameById(@ApiParam("Id de l'agence. Obligatoire.")@PathVariable Integer id) {
		log.debug("Retourne le nom d'une agence en fonction de son Id : {}", id);
		return agencyRepository.findNameById(id);
	}
}
