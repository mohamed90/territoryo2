package com.o2.iris.territoryallocation.web.controller;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.o2.iris.territoryallocation.model.Iris;
import com.o2.iris.territoryallocation.repository.IrisRepository;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class IrisController {

	 private static final Logger log = LoggerFactory.getLogger(IrisController.class);
	
	@Autowired
	private IrisRepository irisRepository;

//	@GetMapping(path="/iris")
//	@ApiOperation("Retourne tous les iris (pagination)")
//	public Page<Iris> getAllIrisPaginated(
//			@RequestParam( value="page") Integer page, @RequestParam( value="size" ) Integer size) {
//		log.debug("Requête pour récupérer tous les iris en paginé");
//		PageRequest pageable = PageRequest.of(page, size);
//		return irisRepository.findAll(pageable);
//	}
	
	@GetMapping(path="/iris")
	@ApiOperation("Retourne tous les iris avec un filtre par département, nom d'iris et nom commune")
	public Set<Iris> getAllIrisPaginated(
			@RequestParam( value="depNum") String depNum
			, @RequestParam( value="irisName" ) String irisName
			, @RequestParam(value="nomCom") String nomCom ) {
		log.debug("Requête pour récupérer tous les iris en paginé");
		return irisRepository.findIrisByFilter(depNum, irisName, nomCom);
	}
	
	@GetMapping(path="/iris/dept-nums")
	@ApiOperation("Retourne tous les numéros de département disponibles")
	public Set<String> getAllAvailableDepts() {
		log.debug("Requête pour récupérer tous les numéros de département disponibles");
		return irisRepository.findAvailableDeptNum();
	}
	
	@GetMapping(path="/iris/nom-com")
	@ApiOperation("Retourne tous les noms de commune disponibles")
	public Set<String> getAllAvailableNomCom(
			@RequestParam( value="depNum") String deptNum) {
		log.debug("Requête pour récupérer tous les noms des communes par département disponibles");
		return irisRepository.findAvailableCommune(deptNum);
	}
	
	@GetMapping(path="/iris-filtered")
	@ApiOperation("Retourne tous les noms d'IRIS disponibles")
	public Set<Iris> getAllAvailableIris(
			@RequestParam( value="depNum") String deptNum
			, @RequestParam( value="nomCom") String nomCom ) {
		log.debug("Requête pour récupérer tous les noms des IRIS par département disponibles");
		return irisRepository.findAvailableIrisByDeptNumAndNomCom(deptNum, "%" + nomCom + "%");
	}
	
//	@GetMapping(path="/iris/{id}")
//	@ApiOperation("Retourne un iris par son id")
//	public Optional<Iris> getById(
//			@ApiParam("id iris")@PathVariable Integer id) {
//		log.debug("Requête pour récupérer un iris par id");
//		return irisRepository.findById(id);
//	}

//	@GetMapping(path="/iris/dept-num/{dept_num}")
//	@ApiOperation("Retourne tous les iris par numéro de département")
//	public List<Iris> getIrisByNumDept(@ApiParam("Numéro du département")@PathVariable String dept_num) {
//		log.debug("Requête pour récupérer tous les iris d'un numéro de département");
//		return irisRepository.findIrisByDeptNum(dept_num);
//	}
	
//	@GetMapping(path="/iris/dept-num-list")
//	@ApiOperation("Retourne tous les iris par plusieurs numéro de département")
//	public List<Iris> getIrisByNumsDept(@ApiParam("Numéros du département")@RequestParam("dept") List<String> dept_num_list) {
//		log.debug("Requête pour récupérer tous les iris de plusieurs numéros de département");
//		return irisRepository.findIrisByDeptNums(dept_num_list);
//	}

//	@GetMapping(path="/iris/dept-name/{dept_name}")
//	@ApiOperation("Retourne tous les iris par nom de département")
//	public List<Iris> getIrisByDept(@ApiParam("Nom du département")@PathVariable String dept_name) {
//		log.debug("Requête pour récupérer tous les iris d'un nom de département");
//		return irisRepository.findIrisByDeptName(dept_name);
//	}
	
//	@GetMapping(path="/iris/dept-name-list")
//	@ApiOperation("Retourne tous les iris par plusieurs noms de département")
//	public List<Iris> getIrisByDepts(@ApiParam("Noms du département")@RequestParam("dept") List<String> dept_name_list) {
//		log.debug("Requête pour récupérer tous les iris de plusieurs numéros de département");
//		return irisRepository.findIrisByDeptNames(dept_name_list);
//	}

//	@GetMapping(path="/agencies/{agcy_id}/iris")
//	@ApiOperation("Retourne tous les iris par id d'agcence")
//	@Cacheable("irisByAgency")
//	public List<Iris> getIrisByAgencyName(@ApiParam("id de l'agence")@PathVariable Integer agcy_id) {
//		log.debug("Requête pour récupérer tous les iris à partir de l'id de l'agence");
//		return irisRepository.findIrisByAgencyId(agcy_id);
//	}

//	@GetMapping(path="/iris/no-agency")
//	@ApiOperation("Retourne tous les iris qui n'ont pas d'agence")
//	public List<Iris> getIrisWithNoAgency() {
//		log.debug("Requête pour récupérer tous les iris qui n'ont pas d'agence");
//		return irisRepository.findIrisWithNoAgency();
//	}

//	@GetMapping(path="/affectation-zones/agencies/{agcy_id}/hs-types/{houseservice_type_id}/iris")
//	@ApiOperation("Retourne tous les iris par id agence et id type de prestation")
//	public List<Iris> findIrisByAgencyIdAndHouseserviceTypeId(@ApiParam("Id de l'agence")@RequestParam("agcy_id") Integer agcy_id
//			, @ApiParam("id Type de prestation")@RequestParam("houseservice_type_id") Integer houseservice_type_id) {
//		log.debug("Requête pour récupérer tous les iris par agence et type de prestation");
//		List<Iris> iris = irisRepository.findIrisByAgencyIdAndHouseserviceTypeId(agcy_id, houseservice_type_id);
//		return iris;
//	}



}
