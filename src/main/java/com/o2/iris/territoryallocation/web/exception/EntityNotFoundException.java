package com.o2.iris.territoryallocation.web.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;


@ResponseStatus(code=HttpStatus.NOT_FOUND, reason="Une ou plusieurs entité n'a pas été trouvée.")
public class EntityNotFoundException extends RuntimeException {
}
