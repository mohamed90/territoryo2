package com.o2.iris.territoryallocation.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.o2.iris.territoryallocation.model.Agency;

@Repository
public interface AgencyRepository extends JpaRepository<Agency, Integer>{
	
	 Optional<Agency> findById(Integer id);
	 Optional<String> findNameById(Integer id);
	 Optional<Agency> findByidOdy(Integer id_ody);
}
