package com.o2.iris.territoryallocation.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.o2.iris.territoryallocation.model.Iris;

@Repository
public interface IrisRepository extends JpaRepository<Iris, Integer>{

	@Query(value="SELECT i from Iris i WHERE i.num_dept=:dept_num")
	List<Iris> findIrisByDeptNum(@Param("dept_num") String dept_num);

	@Query(value="SELECT i from Iris AS i where i.num_dept IN :dept_num_list")
	List<Iris> findIrisByDeptNums(@Param("dept_num_list") List<String> dept_num_list);

	@Query(value="SELECT * from iris i where i.NOM_DEPT=:dept_name", nativeQuery = true)
	List<Iris> findIrisByDeptName(@Param("dept_name") String dept_name);

	@Query(value="SELECT * from iris i where i.NOM_DEPT IN :dept_name_list", nativeQuery = true)
	List<Iris> findIrisByDeptNames(@Param("dept_name_list") List<String> dept_name_list);

	@Query(value="SELECT iris from Agency agcy " + 
			"LEFT JOIN AffectationZone af_zone ON agcy.id=af_zone.agency.id " + 
			"LEFT JOIN Zone zone ON af_zone.zone.id=zone.id " + 
			"LEFT JOIN Iris iris ON zone.id=iris.id " + 
			"where agcy.id=:agcy_id")
	List<Iris> findIrisByAgencyId(@Param("agcy_id") Integer agcy_id);


	@Query(value="SELECT iris.* FROM iris iris " + 
			"LEFT JOIN zone zone ON zone.id=iris.zone_id " + 
			"LEFT OUTER JOIN affectation_zone af_zone ON zone.id=af_zone.zone_id " + 
			"WHERE af_zone.agency_id IS NULL", nativeQuery = true)
	List<Iris> findIrisWithNoAgency();

	@Query(value="SELECT iris FROM Iris AS iris "
			+ "WHERE iris.num_dept  LIKE '%:deptNum%' "
			+ "AND   iris.nom_iris  LIKE '%:irisName%' "
			+ "AND   iris.nom_com   LIKE '%:nomCom%'")
	Set<Iris> findIrisByFilter(@Param("deptNum")  String deptNum
			, @Param("irisName")String irisName
			, @Param("nomCom")  String nomCom);
	
	@Query("SELECT DISTINCT(i.num_dept) FROM Iris AS i")
	Set<String> findAvailableDeptNum();
	
	@Query("SELECT DISTINCT(i.nom_com) FROM Iris AS i " + 
			"WHERE i.num_dept = :deptNum")
	Set<String> findAvailableCommune(@Param("deptNum") String deptNum);
	
	@Query(value="SELECT i FROM Iris AS i " + 
			"WHERE i.num_dept = :deptNum " + 
			"AND i.nom_com LIKE :nomCom")
	Set<Iris> findAvailableIrisByDeptNumAndNomCom(@Param("deptNum") String deptNum
			, @Param("nomCom") String nomCom);

	//	@Query(value="SELECT iris FROM AffectationZone af_zone " + 
	//			"LEFT JOIN Agency as agcy ON af_zone.affectationZoneId.agencyId=agcy.id " + 
	//			"LEFT JOIN Iris as iris ON iris.id=af_zone.affectationZoneId.zoneId " + 
	//			"LEFT JOIN HouseServiceType as hstype ON hstype.id=af_zone.affectationZoneId.houseServiceTypeId " + 
	//			"WHERE agcy.id=:agcy_id AND hstype.id=:houseservice_type_id")
	//	List<Iris> findIrisByAgencyIdAndHouseserviceTypeId(@Param("agcy_id") Integer agcy_id, @Param("houseservice_type_id") Integer houseservice_type_id);

}
