package com.o2.iris.territoryallocation.web.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.o2.iris.territoryallocation.model.Zone;
import com.o2.iris.territoryallocation.repository.ZoneRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class ZoneController {

	 private static final Logger log = LoggerFactory.getLogger(ZoneController.class);
	
	@Autowired
	private ZoneRepository zoneRepository;

	@GetMapping(path="/zones")
	@ApiOperation("Retourne toutes les zones")
	@Cacheable("zonesInMap")
	public List<Zone> getAllZones(
			@ApiParam("Top Right Lng (3.110811343241039) ")@RequestParam(required=true) Double TRLng
			, @ApiParam("Top Right Lat (50.66160605075198)")@RequestParam(required=true) Double TRLat
			, @ApiParam("Bottom Left Lng (3.0112477446082266)")@RequestParam(required=true) Double BLLng
			, @ApiParam("Bottom Left Lat (50.60787463496715)")@RequestParam(required=true) Double BLLat) {
		log.info("Requête pour récupérer toutes les zones");


		return zoneRepository.findZoneInPolygone(TRLng ,TRLat, BLLng, BLLat);
	}	

	@GetMapping(path="/zones/{zone_id}")
	@ApiOperation("Retourne une zone spécifique par son id")
	public Optional<Zone> findById(@ApiParam("Id de la zone.")@PathVariable Integer zone_id) {
		log.info("Requête pour récupérer une zone par id {}", zone_id);
		return zoneRepository.findById(zone_id);
	}

	//	//http://localhost:8080/api/zones/hs-type/menage_repassage?lon=0.196435&lat=48.009491
	//	@ApiOperation("Retourne toutes les zones d'un type/lon/lat spécifique")
	//	@GetMapping(path="zone-affectations/hs-types/{houseservice_type_id}/zones")
	//	public List<Zone> findZonesByCoordinatesAndHsType(@ApiParam("Id du type de presta")@PathVariable Integer houseservice_type_id,
	//			@ApiParam("Longitude")@RequestParam("lon") Double longitude,
	//			@ApiParam("Latitude")@RequestParam("lat") Double latitude) {
	//		log.info("Requête pour récupérer une agence (type : {}) à partir de coordonnées : lon {} / lat {}",houseservice_type_id, longitude, latitude);
	//		return zoneRepository.findZonesByCoordinates(houseservice_type_id,longitude,latitude);
	//	}

	//	//http://localhost:8080/api/zones/hs-type/menage_repassage?lon=0.196435&lat=48.009491
	//	@ApiOperation("Retourne toutes les zones d'une agence")
	//	@GetMapping(path="/agencies/{agcy_id}/zones")
	//	public List<Zone> findZonesByAgency(@ApiParam("Id de l'agence")@PathVariable Integer agcy_id) {
	//		log.info("Requête pour récupérer les zone géographiques associées à une agence {}", agcy_id);
	//		return zoneRepository.findZonesByAgency(agcy_id);
	//	}
}
