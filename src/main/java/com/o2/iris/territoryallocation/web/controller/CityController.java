package com.o2.iris.territoryallocation.web.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.o2.iris.territoryallocation.model.City;
import com.o2.iris.territoryallocation.repository.CityRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class CityController {
	
	 private static final Logger log = LoggerFactory.getLogger(CityController.class);

	@Autowired
	private CityRepository cityRepository;
	
	@GetMapping(path="/cities/{id}")
	@ApiOperation("Retourne une ville par id")
	public Optional<City> getCitiesById(@ApiParam("Id de la ville. Obligatoire.")@PathVariable Integer id) {
		log.debug("Requête pour récupérer une ville à partir de son id : {}", id);
		return cityRepository.findById(id);
	}
	
	@GetMapping(path="/cities")
	@ApiOperation("Retourne toutes les villes")
	public List<City> getAllCities() {
		log.debug("Requête pour récupérer toutes les villes");
		return cityRepository.findAll();
	}
}
