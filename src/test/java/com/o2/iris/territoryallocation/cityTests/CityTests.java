package com.o2.iris.territoryallocation.cityTests;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import com.o2.iris.territoryallocation.model.City;
import com.o2.iris.territoryallocation.web.controller.CityController;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
@ContextConfiguration(classes = CityController.class)

public class CityTests {
	@Mock
	CityController cityMock = mock(CityController.class);
	
	@Test
	public void cityByIdTest() {
		City myCityTest =  new City(1, "City1", null);
		
		when(cityMock.getCitiesById(1)).thenReturn(Optional.of(myCityTest));
		assertEquals(myCityTest,cityMock.getCitiesById(1).get());
	}
	
	@Test
	public void allCitiesTest() {
		ArrayList<City> cities = new ArrayList<City>();
		cities.add(new City(1, "City1", null));
		cities.add(new City(2, "City2", null));
		cities.add(new City(3, "City3", null));
		
		when(cityMock.getAllCities()).thenReturn(cities);
		assertEquals(cities,cityMock.getAllCities());
	}
}
