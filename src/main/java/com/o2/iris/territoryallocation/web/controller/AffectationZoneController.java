package com.o2.iris.territoryallocation.web.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.o2.iris.territoryallocation.model.AffectationZone;
import com.o2.iris.territoryallocation.model.Agency;
import com.o2.iris.territoryallocation.model.HouseServiceType;
import com.o2.iris.territoryallocation.model.Iris;
import com.o2.iris.territoryallocation.model.Zone;
import com.o2.iris.territoryallocation.repository.AffectationZoneRepository;
import com.o2.iris.territoryallocation.repository.AgencyRepository;
import com.o2.iris.territoryallocation.repository.HouseServiceTypeRepository;
import com.o2.iris.territoryallocation.repository.ZoneRepository;
import com.o2.iris.territoryallocation.web.exception.BadRequestException;
import com.o2.iris.territoryallocation.web.exception.EntityNotFoundException;
import com.o2.iris.territoryallocation.web.utils.HeaderUtil;
import com.o2.iris.territoryallocation.web.utils.Mailer;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class AffectationZoneController {

	private static final Logger log = LoggerFactory.getLogger(AffectationZoneController.class);

	private static final String ENTITY_NAME = "affectation-zone";

	@Autowired
	private AffectationZoneRepository affectationZoneRepository;

	@Autowired
	private AgencyRepository agencyRepository;

	@Autowired
	private ZoneRepository zoneRepository;

	@Autowired
	private HouseServiceTypeRepository houseServiceTypeRepository;

	@Autowired
	private Environment environment;

	@GetMapping(path = "/sendMail")
	public String sendMail() throws Exception {

		log.debug("Appel de l'url du serveur de messages");

		// Envoyer la requête URL finale à l'attention du serveur de message
		List<String> tos = new ArrayList<>();
		tos.add("alban.rousseau@o2.fr");
		
		String profile = environment.getProperty("spring.profiles.active");
		String telegestUrl = environment.getProperty("telegest.url");

		Mailer mail = new Mailer(profile, telegestUrl)
				.setFrom("alban.rousseau@test.o2.fr")
				.setTo(tos)
				.setType("1")
				.setMessage("<div>\r\n" + 
						"    Bonjour,\r\n" + 
						"</div> \r\n" + 
						"\r\n" + 
						"<div>une zone a été affectée à l'agence <b>O2 Le Mans Nord</b>.</div>\r\n" + 
						"\r\n" + 
						"<div>\r\n" + 
						"    <ul>\r\n" + 
						"        <li>Nom de la Zone : <b>Bellevue</b></li>\r\n" + 
						"        <li>Commune : <b>Coulaine</b></li>\r\n" + 
						"        <li>Date de début d'affectation : <b>01/01/01</b></li>\r\n" + 
						"        <li>Date de fin d'affectation : <b>01/01/01</b></li>\r\n" + 
						"        <li>\r\n" + 
						"            Prestations affectées :\r\n" + 
						"            <ol type=\"1\">\r\n" + 
						"                <li>Ménage Repassage</li>\r\n" + 
						"                <li>Garde d'Enfant</li>\r\n" + 
						"                <li>Bricolage</li>\r\n" + 
						"            </ol>\r\n" + 
						"        </li>\r\n" + 
						"    </ul>\r\n" + 
						"</div>\r\n" + 
						"\r\n" + 
						"<div>\r\n" + 
						"   Cordialement. \r\n" + 
						"</div>\r\n" + 
						"")
				.setObjet("Nouvelle zone affectée pour O2 Le Mans Nord");
		
		String res = mail.send();

		return res;
	}

	@GetMapping(path = "/affectation-zones")
	@ApiOperation("Retourne toutes les zones d'affectation")
	public List<AffectationZone> getAllAffectationZones() {
		log.debug("Requête pour récupérer toutes les zones d'affectation");
		return affectationZoneRepository.findAll();
	}

	@GetMapping(path = "/affectation-zones/{id}")
	@ApiOperation("Retourne une affectation-zones par id")
	public Optional<AffectationZone> getAffectationZoneById(
			@ApiParam("Id de la zone d'affectation. Obligatoire.") @PathVariable Integer id) {
		log.debug("Requête pour récupérer une affectation-zones à partir de son id : {}", id);
		return affectationZoneRepository.findById(id);
	}

	@GetMapping(path = "/agencies/{agcy_id}/affectation-zones")
	@ApiOperation("Retourne toutes les zones d'affectation par agence")
	@Cacheable(value = "affectationZonesByAgency", key = "#agcy_id")
	public List<AffectationZone> getAllAffectationZonesByAgency(
			@ApiParam("id de l'agence") @PathVariable Integer agcy_id) {
		log.debug("Requête pour récupérer toutes les zones d'affectation d'une agence");
		return affectationZoneRepository.getAllAffectationZonesByAgency(agcy_id);
	}

	// http://localhost:8080/api/affectation-zones/hs-type/menage_repassage?lon=0.196435&lat=48.009491
	@GetMapping(path = "/affectation-zones-by-hs-types")
	@ApiOperation("Retourne une zone d'affectation par coordonnée longitude/latitude et type de presta")
	public List<AffectationZone> getZonesByCoordinates(
			@ApiParam("shortname Type de prestation (menage)") @RequestParam(value = "hst_sn", required = true) Set<String> hst_sn,
			@ApiParam("Longitude (0.194214)") @RequestParam(value = "lon", required = true) Double longitude,
			@ApiParam("Latitude (47.995388)") @RequestParam(value = "lat", required = true) Double latitude) {
		log.debug("Requête pour récupérer une agence à partir de coordonnées et sn type de presta");

		List<AffectationZone> affectationZones = affectationZoneRepository.findAffectationZonesByCoordinates(longitude,
				latitude);
		List<AffectationZone> affectationZonesResult = new ArrayList<>();

		for (AffectationZone affectationZone : affectationZones) {
			if (!Collections.disjoint(hst_sn, affectationZone.getHsTypeIdList())) {
				affectationZonesResult.add(affectationZone);
			}
		}

		return affectationZonesResult;
	}
	
	//countZonesByAgency
	@GetMapping(path="agencies/{id_ody}/affectation-zones/count")
	@ApiOperation("Retourne le nombre de zones affectées à une agence")
	public Long countZonesByAgency(@ApiParam("Id Ody de l'agence. Obligatoire.")@PathVariable Integer id_ody) {
		log.debug("Requête pour retourner le nombre de zones affectées à une agence : {}", id_ody);
		return affectationZoneRepository.countZonesByAgencyOdy(id_ody);
	}
	
	@PostMapping(path = "/affectation-zones")
	@ApiOperation("Ajoute une zone d'affectation")
	@CacheEvict(value = "affectationZonesByAgency", key = "#agency_id")
	public ResponseEntity<AffectationZone> addAffectationZone(@RequestParam("zone_id") Integer zone_id,
			@RequestParam("agency_id") Integer agency_id,
			@RequestParam(value = "hst_sn", required = false) Set<String> hst_sn,
			@RequestParam("start_datetime") String start_datetime, @RequestParam("end_datetime") String end_datetime)
			throws URISyntaxException {

		Optional<Agency> agency = agencyRepository.findById(agency_id);
		Optional<Zone> zone = zoneRepository.findById(zone_id);

		Optional<AffectationZone> optAlreadyAffectedZone = findHstAlreadyAffected(zone_id, hst_sn, null);

		if (optAlreadyAffectedZone.isPresent()) {
			return ResponseEntity.accepted().body(optAlreadyAffectedZone.get());
		}

		Set<HouseServiceType> houseServiceTypes = new HashSet<>();

		if (hst_sn != null) {
			for (String sn : hst_sn) {
				houseServiceTypes.add(houseServiceTypeRepository.findByshortname(sn));
			}

			if (hst_sn.contains("menage_repassage")) {
				houseServiceTypes.add(houseServiceTypeRepository.findByshortname("menage"));
				houseServiceTypes.add(houseServiceTypeRepository.findByshortname("repassage"));
			}
		}

		LocalDate startDatetime = LocalDate.parse(start_datetime, DateTimeFormatter.ISO_LOCAL_DATE);
		LocalDate endDatetime = LocalDate.parse(end_datetime, DateTimeFormatter.ISO_LOCAL_DATE);

		if (agency.get() == null || zone.get() == null)
			return ResponseEntity.noContent().build();

		AffectationZone affectationZoneAdded = new AffectationZone(agency.get(), houseServiceTypes, zone.get(),
				startDatetime, endDatetime);

		affectationZoneAdded = affectationZoneRepository.save(affectationZoneAdded);
		Long nbAffectationZone = affectationZoneRepository.countZonesByAgencyOdy(agency.get().getIdOdy());
		
		log.debug("Appel de l'url du serveur de messages");

		// Envoyer la requête URL finale à l'attention du serveur de message
		List<String> tos = new ArrayList<>();
		tos.add("alban.rousseau@o2.fr");
		
		String profile = environment.getProperty("spring.profiles.active");
		String telegestUrl = environment.getProperty("telegest.url");
		
		String hstypes = "";
		
		for(HouseServiceType hst: affectationZoneAdded.getHouseServiceTypes()) {
			if(null != hst) {
				hstypes += "<li>"+hst.getLibelle()+"</li>";
			} else {
				hstypes += "<li>NULL SERVICE</li>";
			}
			
		}

		Mailer mail = new Mailer(profile, telegestUrl)
				.setFrom("alban.rousseau@test.o2.fr")
				.setTo(tos)
				.setType("1")
				.setMessage("<div>\r\n" + 
						"    Bonjour,\r\n" + 
						"</div> \r\n" + 
						"\r\n" + 
						"<div>une zone a été affectée à l'agence <b>"+affectationZoneAdded.getAgency().getName()+"</b>.</div>\r\n" + 
						"\r\n" + 
						"<div>\r\n" + 
						"    <ul>\r\n" + 
						"        <li>Nom de la Zone : <b>"+affectationZoneAdded.getZone().getName()+"</b></li>\r\n" + 
						"        <li>Commune : <b>"+((Iris)affectationZoneAdded.getZone()).getNom_dept()+"</b></li>\r\n" + 
						"        <li>Date de début d'affectation : <b>"+affectationZoneAdded.getStart_datetime()+"</b></li>\r\n" + 
						"        <li>Date de fin d'affectation : <b>"+affectationZoneAdded.getEnd_datetime()+"</b></li>\r\n" + 
						"        <li>\r\n" + 
						"            Prestations affectées :\r\n" + 
						"            <ol type=\"1\">\r\n" + 
						hstypes +
						"            </ol>\r\n" + 
						"        </li>\r\n" + 
						"    </ul>\r\n" + 
						"</div>\r\n" + 
						"\r\n" + 
						"<div>\r\n" + 
						"Désormais, l'agence "+affectationZoneAdded.getAgency().getName()+" est affectée à "+nbAffectationZone+" zones.\r\n" + 
						"</div>\r\n" + 
						
						"<div>\r\n" + 
						"   Cordialement. \r\n" + 
						"</div>\r\n" + 
						"")
				.setObjet("Nouvelle zone affectée pour "+ affectationZoneAdded.getAgency().getName());
		
		String res = mail.send();

		return ResponseEntity.created(new URI("/api/affectation-zones/" + affectationZoneAdded.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, affectationZoneAdded.getId().toString()))
				.body(affectationZoneAdded);

	}

	private Optional<AffectationZone> findHstAlreadyAffected(Integer zone_id, Set<String> hst_sn, Integer idAZ) {

		if (hst_sn != null) {
			for (String sn : hst_sn) {
				Optional<AffectationZone> optAZ = affectationZoneRepository.isHstAlreadyAffected(zone_id,
						houseServiceTypeRepository.findByshortname(sn));
				if (optAZ.isPresent()) {
					if (idAZ == null || idAZ != optAZ.get().getId()) {
						log.info("Zone d'affectation trouvée : {}, pour la zone d'id : {}, type de presta : {}",
								optAZ.get(), zone_id, sn);
						return optAZ;
					}
				}
			}
		}
		return Optional.empty();
	}

	@DeleteMapping(path = "/affectation-zones/{affectation_zone_id}")
	@ApiOperation("Supprime une zone d'affectation.")
	@CacheEvict(value = "affectationZonesByAgency", key = "#agency_id")
	public ResponseEntity<Void> deleteAffectationZone(@PathVariable Integer affectation_zone_id,
			@RequestParam("agency_id") Integer agency_id) {
		Optional<AffectationZone> optAffectationZone;

		if (affectation_zone_id == null)
			throw new BadRequestException("Id d'une zone d'affectation attendu.");
		else {
			optAffectationZone = affectationZoneRepository.findById(affectation_zone_id);
			if (optAffectationZone.isPresent()) {

				List<Integer> azhstIds = affectationZoneRepository.getAllAffectationZoneIdsByAgencyAndZone(
						optAffectationZone.get().getAgency().getId(), optAffectationZone.get().getZone().getId());
				affectationZoneRepository.deleteJoinedTable(azhstIds);

				affectationZoneRepository.delete(optAffectationZone.get());
				
				Optional<Agency> agency = agencyRepository.findById(agency_id);
				Long nbAffectationZone = affectationZoneRepository.countZonesByAgencyOdy(agency.get().getIdOdy());
				
				log.debug("Appel de l'url du serveur de messages");

				// Envoyer la requête URL finale à l'attention du serveur de message
				List<String> tos = new ArrayList<>();
				tos.add("alban.rousseau@o2.fr");
				
				String profile = environment.getProperty("spring.profiles.active");
				String telegestUrl = environment.getProperty("telegest.url");
				
				String hstypes = "";
				
				for(HouseServiceType hst: optAffectationZone.get().getHouseServiceTypes()) {
					if(null != hst) {
						hstypes += "<li>"+hst.getLibelle()+"</li>";
					} else {
						hstypes += "<li>NULL SERVICE</li>";
					}
					
				}

				Mailer mail = new Mailer(profile, telegestUrl)
						.setFrom("alban.rousseau@test.o2.fr")
						.setTo(tos)
						.setType("1")
						.setMessage("<div>\r\n" + 
								"    Bonjour,\r\n" + 
								"</div> \r\n" + 
								"\r\n" + 
								"<div>une zone a été supprimée pour l'agence <b>"+optAffectationZone.get().getAgency().getName()+"</b>.</div>\r\n" + 
								"\r\n" + 
								"<div>\r\n" + 
								"    <ul>\r\n" + 
								"        <li>Nom de la Zone : <b>"+optAffectationZone.get().getZone().getName()+"</b></li>\r\n" + 
								"        <li>Commune : <b>"+((Iris)optAffectationZone.get().getZone()).getNom_dept()+"</b></li>\r\n" + 
								"        <li>Date de début d'affectation : <b>"+optAffectationZone.get().getStart_datetime()+"</b></li>\r\n" + 
								"        <li>Date de fin d'affectation : <b>"+optAffectationZone.get().getEnd_datetime()+"</b></li>\r\n" +  
								"    </ul>\r\n" + 
								"</div>\r\n" + 
								"\r\n" + 
								"<div>\r\n" + 
								"Désormais, l'agence "+optAffectationZone.get().getAgency().getName()+" est affectée à "+nbAffectationZone+" zones.\r\n" + 
								"</div>\r\n" +
								"<div>\r\n" + 
								"   Cordialement. \r\n" + 
								"</div>\r\n" + 
								"")
						.setObjet("Suppression d'une zone affectée pour "+ optAffectationZone.get().getAgency().getName());
				
				String res = mail.send();
				
				return ResponseEntity.ok()
						.headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, affectation_zone_id.toString()))
						.build();
			} else {
				throw new EntityNotFoundException();
			}
		}

	}
	// @DeleteMapping(path="/affectation-zones")
	// @ApiOperation("Supprime les zones d'affectation par agence/zone")
	// public ResponseEntity<?>
	// deleteAffectationZoneByAgcy_Zone(@RequestParam(value="zone_id",
	// required=false) Integer zone_id
	// , @RequestParam("agency_id") Integer agency_id
	// ){
	//
	// Optional<Agency> agency;
	// Optional<Zone> zone;
	//
	// if(agency_id == null && zone_id == null)
	// throw new BadRequestException("Cette requête attend au moins une agence et
	// une zone ou une agence et un type de presta en paramètre");
	// else if(agency_id != null && zone_id != null) {
	// agency = agencyRepository.findById(agency_id);
	// zone = zoneRepository.findById(zone_id);
	// if(agency.isPresent() && zone.isPresent()) {
	//
	// List<Integer> azhstIds =
	// affectationZoneRepository.getAllAffectationZoneIdsByAgencyAndZone(agency_id,
	// zone_id);
	// affectationZoneRepository.deleteJoinedTable(azhstIds);
	//
	// affectationZoneRepository.deleteAffectationZoneByAgcy_Zone(zone.get().getId(),
	// agency.get().getId());
	// return ResponseEntity.ok().build();
	// }else {
	// throw new EntityNotFoundException();
	// }
	// }else if(agency_id != null && zone_id == null) {
	// agency = agencyRepository.findById(agency_id);
	//
	// if(agency.isPresent()) {
	//
	// List<Integer> azhstIds =
	// affectationZoneRepository.getAllAffectationZoneIdsByAgency(agency_id);
	// affectationZoneRepository.deleteJoinedTable(azhstIds);
	//
	// affectationZoneRepository.deleteAffectationZoneByAgcy(agency.get().getId());
	// return ResponseEntity.ok().build();
	// }else {
	// throw new EntityNotFoundException();
	// }
	//
	// }else {
	// throw new BadRequestException("Cette requête attend au moins une agence et
	// une zone");
	// }
	//
	// }

	@PutMapping(path = "/affectation-zones")
	@ApiOperation("Modifie une zone d'affectation")
	@CacheEvict(value = "affectationZonesByAgency", key = "#agency_id")
	public ResponseEntity<AffectationZone> updateAffectationZone(
			@RequestParam("affectation_zone_id") Integer affectation_zone_id,
			@RequestParam("agency_id") Integer agency_id,
			@RequestParam(value = "hst_sn", required = false) Set<String> hst_sn,
			@RequestParam(value = "start_datetime", required = false) String start_datetime,
			@RequestParam(value = "end_datetime", required = false) String end_datetime) throws URISyntaxException {

		if (agency_id == null && affectation_zone_id == null) {
			throw new BadRequestException(
					"L'id de la zone d'affectation ainsi que l'id de l'agence (pour le cache) sont obligatoires");
		}

		if (hst_sn == null && start_datetime == null && end_datetime == null) {
			throw new BadRequestException(
					"Une date de début, de fin, ou une liste de prestation doit être renseignée pour la mise à jour de l'affectation.");
		}

		Optional<AffectationZone> optAffectationZone = affectationZoneRepository.findById(affectation_zone_id);

		if (optAffectationZone.isPresent()) {
			
			String hstypesOld = "";
			for(HouseServiceType hstOld: optAffectationZone.get().getHouseServiceTypes()) {
				if(null != hstOld) {
					hstypesOld += "<li>"+hstOld.getLibelle()+"</li>";
				} else {
					hstypesOld += "<li>NULL SERVICE</li>";
				}
			}
			
			Optional<AffectationZone> optAlreadyAffectedZone = findHstAlreadyAffected(
					optAffectationZone.get().getZone().getId(), hst_sn, optAffectationZone.get().getId());

			if (optAlreadyAffectedZone.isPresent()) {
				return ResponseEntity.accepted().body(optAlreadyAffectedZone.get());
			}

			Set<HouseServiceType> houseServiceTypes = new HashSet<>();
			if (hst_sn != null) {
				for (String sn : hst_sn) {
					if (!sn.equals("menage") && !sn.equals("repassage"))
						houseServiceTypes.add(houseServiceTypeRepository.findByshortname(sn));
				}

				if (hst_sn.contains("menage_repassage")) {
					houseServiceTypes.add(houseServiceTypeRepository.findByshortname("menage"));
					houseServiceTypes.add(houseServiceTypeRepository.findByshortname("repassage"));
				}

			}
			optAffectationZone.get().setHouseServiceTypes(houseServiceTypes);

			if (start_datetime != null) {
				LocalDate startDatetime = LocalDate.parse(start_datetime, DateTimeFormatter.ISO_LOCAL_DATE);
				optAffectationZone.get().setStart_datetime(startDatetime);
			}

			if (end_datetime != null) {
				LocalDate endDatetime = LocalDate.parse(end_datetime, DateTimeFormatter.ISO_LOCAL_DATE);
				optAffectationZone.get().setEnd_datetime(endDatetime);
			}

			AffectationZone updtatedAffectationZone = affectationZoneRepository.save(optAffectationZone.get());
			
			Optional<Agency> agency = agencyRepository.findById(agency_id);
			Long nbAffectationZone = affectationZoneRepository.countZonesByAgencyOdy(agency.get().getIdOdy());

			log.debug("Appel de l'url du serveur de messages");

			// Envoyer la requête URL finale à l'attention du serveur de message
			List<String> tos = new ArrayList<>();
			tos.add("alban.rousseau@o2.fr");
			
			String profile = environment.getProperty("spring.profiles.active");
			String telegestUrl = environment.getProperty("telegest.url");
			
			String hstypes = "";
			for(HouseServiceType hst: updtatedAffectationZone.getHouseServiceTypes()) {
				if(null != hst) {
					hstypes += "<li>"+hst.getLibelle()+"</li>";
				} else {
					hstypes += "<li>NULL SERVICE</li>";
				}
				
			}

			Mailer mail = new Mailer(profile, telegestUrl)
					.setFrom("alban.rousseau@test.o2.fr")
					.setTo(tos)
					.setType("1")
					.setMessage("<div>\r\n" + 
							"    Bonjour,\r\n" + 
							"</div> \r\n" + 
							"\r\n" + 
							"<div>une zone a été modifiée pour l'agence <b>"+updtatedAffectationZone.getAgency().getName()+"</b>.</div>\r\n" + 
							"\r\n" + 
							"<div>\r\n" + 
							"    <ul>\r\n" + 
							"        <li>Nom de la Zone : <b>"+updtatedAffectationZone.getZone().getName()+"</b></li>\r\n" + 
							"        <li>Commune : <b>"+((Iris)updtatedAffectationZone.getZone()).getNom_dept()+"</b></li>\r\n" + 
							"        <li>Date de début d'affectation : <b>"+updtatedAffectationZone.getStart_datetime()+"</b></li>\r\n" + 
							"        <li>Date de fin d'affectation : <b>"+updtatedAffectationZone.getEnd_datetime()+"</b></li>\r\n" + 
							"        <li>\r\n" + 
							"            Anciennes Prestations :\r\n" + 
							"            <ol type=\"1\">\r\n" + 
							hstypesOld +
							"            </ol>\r\n" + 
							"        </li>\r\n" +
							"        <li>\r\n" + 
							"            Nouvelles Prestations affectées :\r\n" + 
							"            <ol type=\"1\">\r\n" + 
							hstypes +
							"            </ol>\r\n" + 
							"        </li>\r\n" + 
							"    </ul>\r\n" + 
							"</div>\r\n" + 
							"\r\n" + 
							"<div>\r\n" + 
							"Désormais, l'agence "+updtatedAffectationZone.getAgency().getName()+" est affectée à "+nbAffectationZone+" zones.\r\n" + 
							"</div>\r\n" +
							"<div>\r\n" + 
							"   Cordialement. \r\n" + 
							"</div>\r\n" + 
							"")
					.setObjet("Mise à jour d'une zone affectée pour "+ updtatedAffectationZone.getAgency().getName());
			
			String res = mail.send();
			
			return ResponseEntity
					.created(new URI("/api/affectation-zones/" + updtatedAffectationZone.getId())).headers(HeaderUtil
							.createEntityCreationAlert(ENTITY_NAME, updtatedAffectationZone.getId().toString()))
					.body(updtatedAffectationZone);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}
