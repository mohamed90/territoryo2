package com.o2.iris.territoryallocation.affectationZoneTests;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.o2.iris.territoryallocation.model.AffectationZone;
import com.o2.iris.territoryallocation.model.Agency;
import com.o2.iris.territoryallocation.model.HouseServiceType;
import com.o2.iris.territoryallocation.model.Iris;
import com.o2.iris.territoryallocation.model.Zone;
import com.o2.iris.territoryallocation.repository.AffectationZoneRepository;
import com.o2.iris.territoryallocation.repository.AgencyRepository;
import com.o2.iris.territoryallocation.repository.HouseServiceTypeRepository;
import com.o2.iris.territoryallocation.repository.ZoneRepository;
import com.o2.iris.territoryallocation.web.controller.AffectationZoneController;

@RunWith(SpringRunner.class)
@WebMvcTest(AffectationZoneController.class)
public class AffectationZoneTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AgencyRepository agencyRepository;

	@MockBean
	private AffectationZoneRepository affectationZoneRepository;

	@MockBean
	private ZoneRepository zoneRepository;

	@MockBean
	private HouseServiceTypeRepository houseServiceTypeRepository;

	ArrayList<Agency> agencies = new ArrayList<Agency>();
	ArrayList<HouseServiceType> hstList = new ArrayList<HouseServiceType>();
	Date start_datetime = new Date();
	Date end_datetime = new Date();

	ResponseEntity<AffectationZone> reponseAffectationZone;

	private Agency a;

	private HashSet<HouseServiceType> hsts;

	private Zone z;

	private AffectationZone aa;

	private HouseServiceType hst;

	private LocalDate startDatetime;

	private LocalDate endDatetime;

	@Before
	public void before() {

		hsts = new HashSet<HouseServiceType>();

		// Mock for Agencies
		a = new Agency();
		a.setId(631);
		a.setIdOdy(1);
		a.setName("O2 Le Mans");
		when(agencyRepository.findById(631)).thenReturn(Optional.of(a));

		// Mock for zone
		z = new Iris();
		z.setId(171639);
		when(zoneRepository.findById(171639)).thenReturn(Optional.of(z));

		startDatetime = LocalDate.parse("2018-09-06", DateTimeFormatter.ISO_LOCAL_DATE);
		endDatetime = LocalDate.parse("2018-09-06", DateTimeFormatter.ISO_LOCAL_DATE);

	}

	@Test
	public void newAffectationZoneShouldReturnAffectationZoneTest() throws Exception {

		// mock for HouseServiceType
		hst = new HouseServiceType();
		hst.setId(1);
		hst.setShortname("garde_enfant");
		when(houseServiceTypeRepository.findByshortname("garde_enfant")).thenReturn(hst);

		hsts.add(hst);

		aa = new AffectationZone(a, hsts, z, startDatetime, endDatetime);
		aa.setId(1);
		when(affectationZoneRepository.save(aa)).thenReturn(aa);

		// Dans le cas où il n'y a pas de doublon
		when(affectationZoneRepository.isHstAlreadyAffected(171639, hst)).thenReturn(Optional.empty());

		MvcResult mvcResult = mockMvc.perform(post("/api/affectation-zones").param("zone_id", "171639").param("agency_id", "631")
				.param("hst_sn", "garde_enfant").param("start_datetime", "2018-09-06")
				.param("end_datetime", "2018-09-06")).andDo(print()).andExpect(status().isCreated())

				.andExpect(jsonPath("$.id", is(1))).andExpect(jsonPath("$.start_datetime", is("2018-09-06")))
				.andExpect(jsonPath("$.end_datetime", is("2018-09-06")))

				.andExpect(jsonPath("$.agency.id", is(a.getId())))
				.andExpect(jsonPath("$.agency.idOdy", is(a.getIdOdy())))
				.andExpect(jsonPath("$.agency.disabled", is(a.isDisabled())))
				.andExpect(jsonPath("$.agency.name", is(a.getName())))

				.andExpect(jsonPath("$.houseServiceTypes[0].id", is(1)))
				.andExpect(jsonPath("$.houseServiceTypes[0].shortname", is("garde_enfant")))

				.andExpect(jsonPath("$.zone.id", is(z.getId()))).andReturn()

		;
		
		String XTerritoryAllocationAlert = mvcResult.getResponse().getHeader("X-territoryAllocation-alert");
		assertEquals("territoryAllocation.affectation-zone.created", XTerritoryAllocationAlert);
		String XTerritoryAllocationParams = mvcResult.getResponse().getHeader("X-territoryAllocation-params");
		assertEquals("1", XTerritoryAllocationParams);
		String contentType = mvcResult.getResponse().getHeader("Content-Type");
		assertEquals("application/json;charset=UTF-8", contentType);
		String location = mvcResult.getResponse().getHeader("Location");
		assertEquals("/api/affectation-zones/1", location);

	}

	@Test
	public void newAffectationZoneWithMenageRepassageShouldReturnAffectationZoneWithMenageAndRepassageTest()
			throws Exception {

		// mock for HouseServiceType
		hst = new HouseServiceType();
		hst.setId(1);
		hst.setShortname("menage_repassage");
		hsts.add(hst);
		when(houseServiceTypeRepository.findByshortname("menage_repassage")).thenReturn(hst);

		hst = new HouseServiceType();
		hst.setId(2);
		hst.setShortname("menage");
		hsts.add(hst);
		when(houseServiceTypeRepository.findByshortname("menage")).thenReturn(hst);

		hst = new HouseServiceType();
		hst.setId(3);
		hst.setShortname("repassage");
		hsts.add(hst);
		when(houseServiceTypeRepository.findByshortname("repassage")).thenReturn(hst);

		aa = new AffectationZone(a, hsts, z, startDatetime, endDatetime);
		aa.setId(1);
		when(affectationZoneRepository.save(aa)).thenReturn(aa);

		// Dans le cas où il n'y a pas de doublon
		when(affectationZoneRepository.isHstAlreadyAffected(z.getId(), hst)).thenReturn(Optional.empty());

		MvcResult mvcResult = mockMvc.perform(post("/api/affectation-zones").param("zone_id", z.getId().toString())
				.param("agency_id", a.getId().toString()).param("hst_sn", "menage_repassage")
				.param("start_datetime", "2018-09-06").param("end_datetime", "2018-09-06")).andDo(print())
				.andExpect(status().isCreated()).andExpect(jsonPath("$.id", is(1)))
				.andExpect(jsonPath("$.start_datetime", is("2018-09-06")))
				.andExpect(jsonPath("$.end_datetime", is("2018-09-06")))

				.andExpect(jsonPath("$.agency.id", is(a.getId())))
				.andExpect(jsonPath("$.agency.idOdy", is(a.getIdOdy())))
				.andExpect(jsonPath("$.agency.disabled", is(a.isDisabled())))
				.andExpect(jsonPath("$.agency.name", is(a.getName())))

				.andExpect(jsonPath("$.houseServiceTypes", hasSize(3)))

				.andExpect(jsonPath("$.zone.id", is(z.getId()))).andReturn()

		;
		
		String XTerritoryAllocationAlert = mvcResult.getResponse().getHeader("X-territoryAllocation-alert");
		assertEquals("territoryAllocation.affectation-zone.created", XTerritoryAllocationAlert);
		String XTerritoryAllocationParams = mvcResult.getResponse().getHeader("X-territoryAllocation-params");
		assertEquals("1", XTerritoryAllocationParams);
		String contentType = mvcResult.getResponse().getHeader("Content-Type");
		assertEquals("application/json;charset=UTF-8", contentType);
		String location = mvcResult.getResponse().getHeader("Location");
		assertEquals("/api/affectation-zones/1", location);

	}

	@Test
	public void modifiedAffectationZoneShouldReturnAffectationZoneWithSameId() throws Exception {
		// mock for HouseServiceType
		hst = new HouseServiceType();
		hst.setId(1);
		hst.setShortname("menage_repassage");
		hsts.add(hst);
		when(houseServiceTypeRepository.findByshortname("menage_repassage")).thenReturn(hst);

		hst = new HouseServiceType();
		hst.setId(2);
		hst.setShortname("menage");
		hsts.add(hst);
		when(houseServiceTypeRepository.findByshortname("menage")).thenReturn(hst);

		hst = new HouseServiceType();
		hst.setId(3);
		hst.setShortname("repassage");
		hsts.add(hst);
		when(houseServiceTypeRepository.findByshortname("repassage")).thenReturn(hst);

		AffectationZone azBefore = new AffectationZone(a, hsts, z, startDatetime, endDatetime);
		azBefore.setId(1);

		// ajout d'un type de prestation à la zone d'affectation
		hst = new HouseServiceType();
		hst.setId(4);
		hst.setShortname("garde_enfant");
		hsts.add(hst);
		when(houseServiceTypeRepository.findByshortname("garde_enfant")).thenReturn(hst);

		AffectationZone azAfter = new AffectationZone(a, hsts, z, startDatetime, endDatetime);
		azAfter.setId(1);

		when(affectationZoneRepository.findById(1)).thenReturn(Optional.of(azBefore));
		when(affectationZoneRepository.save(azBefore)).thenReturn(azAfter);

		MvcResult mvcResult = mockMvc.perform(put("/api/affectation-zones").param("affectation_zone_id", azBefore.getId().toString())
				.param("agency_id", a.getId().toString()).param("hst_sn", "menage_repassage")
				.param("hst_sn", "garde_enfant").param("start_datetime", "2018-09-06")
				.param("end_datetime", "2018-09-06")).andDo(print()).andExpect(status().isCreated())
				.andExpect(jsonPath("$.id", is(azBefore.getId())))
				.andExpect(jsonPath("$.start_datetime", is("2018-09-06")))
				.andExpect(jsonPath("$.end_datetime", is("2018-09-06")))

				.andExpect(jsonPath("$.agency.id", is(a.getId())))
				.andExpect(jsonPath("$.agency.idOdy", is(a.getIdOdy())))
				.andExpect(jsonPath("$.agency.disabled", is(a.isDisabled())))
				.andExpect(jsonPath("$.agency.name", is(a.getName())))

				.andExpect(jsonPath("$.houseServiceTypes", hasSize(4)))

				.andExpect(jsonPath("$.zone.id", is(z.getId()))).andReturn();
		
		String XTerritoryAllocationAlert = mvcResult.getResponse().getHeader("X-territoryAllocation-alert");
		assertEquals("territoryAllocation.affectation-zone.created", XTerritoryAllocationAlert);
		String XTerritoryAllocationParams = mvcResult.getResponse().getHeader("X-territoryAllocation-params");
		assertEquals("1", XTerritoryAllocationParams);
		String contentType = mvcResult.getResponse().getHeader("Content-Type");
		assertEquals("application/json;charset=UTF-8", contentType);
		String location = mvcResult.getResponse().getHeader("Location");
		assertEquals("/api/affectation-zones/1", location);

	}

	@Test
	public void deleteExistingAffectationZoneShouldReturnOk() throws Exception {

		// mock for HouseServiceType
		hst = new HouseServiceType();
		hst.setId(1);
		hst.setShortname("garde_enfant");
		when(houseServiceTypeRepository.findByshortname("garde_enfant")).thenReturn(hst);

		AffectationZone azToDelete = new AffectationZone(a, hsts, z, startDatetime, endDatetime);
		azToDelete.setId(1);

		when(affectationZoneRepository.findById(1)).thenReturn(Optional.of(azToDelete));
		
		List<Integer> azhstIds = new ArrayList<>();
		azhstIds.add(hst.getId());
		when(affectationZoneRepository.getAllAffectationZoneIdsByAgencyAndZone(a.getId(), z.getId())).thenReturn(azhstIds);
		
		MvcResult mvcResult = mockMvc.perform(delete("/api/affectation-zones/1")
				.param("agency_id", a.getId().toString())).andDo(print())
				.andExpect(status().isOk()).andReturn();
		String XTerritoryAllocationAlert = mvcResult.getResponse().getHeader("X-territoryAllocation-alert");
		assertEquals("territoryAllocation.affectation-zone.deleted", XTerritoryAllocationAlert);
		String XTerritoryAllocationParams = mvcResult.getResponse().getHeader("X-territoryAllocation-params");
		assertEquals("1", XTerritoryAllocationParams);
		
	}

}
